#!/usr/bin/python

# Python Version Extractor
# Uses the git describe command to extract version info for current project
# Usage: `./create_version_header.py output.h`

import subprocess
import re

def extract_git_version(projectGitDirectory):

	# Execute git describe command
	p = subprocess.Popen('git describe --dirty=+', stdout=subprocess.PIPE, shell=True, cwd=projectGitDirectory)
	fullVersionString = p.communicate()[0]
	
	# Strip endline
	fullVersionString = fullVersionString.replace('\n', '').replace('\r', '');

	# Extract version numbers
	regex = re.match('v([0-9]+).([0-9]+).([0-9]+)[-]?([0-9]*)', fullVersionString);
	majorVersionString = regex.group(1)
	minorVersionString = regex.group(2)
	patchVersionString = regex.group(3)
	tweakVersionString = regex.group(4).zfill(1)

	# Compute version integer
	versionString = str((int(majorVersionString) << 24) + (int(minorVersionString) << 16) + (int(patchVersionString) << 8) + (int(tweakVersionString) << 0));

	# Check if build is dirty
	regex = re.search('\++$', fullVersionString)
	if (regex is not None):
		dirty = True
	else:
		dirty = False

	return fullVersionString, versionString, majorVersionString, minorVersionString, patchVersionString, tweakVersionString, dirty