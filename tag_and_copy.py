#!/usr/bin/python
 
# Python Binary Tagger and Copier
# Tags a binary file with a version and copies it to an output folder
# Usage: `./tag_and_copy.py output.h`

import os 
import re
import shutil
import argparse
import fetch_version
 
# Parse input argument
parser = argparse.ArgumentParser(description='Generate `git describe` based version header file')
parser.add_argument('projectSourcePath', type=str, help='location of project base directory')
parser.add_argument('projectOutputFilename', type=str, help='name of project output file')
parser.add_argument('projectOutputFilenameExtension', type=str, help='output file extension (e.g. .bin')
 
args = parser.parse_args()
projectSourcePath = args.projectSourcePath
projectOutputFilename = args.projectOutputFilename
projectOutputFilenameExtension = args.projectOutputFilenameExtension

# Extract version strings 
fullVersionString, versionString, majorVersionString, minorVersionString, patchVersionString, tweakVersionString, dirty = fetch_version.extract_git_version(projectSourcePath)

# Create our name tag
versionTag = '-v' + majorVersionString + '.' + minorVersionString + '.' + patchVersionString + '-' + tweakVersionString
if dirty:
	versionTag += '+'	

buildPath = os.getcwd()
if os.getenv("OUTPUT_BIN_PATH"):
    outputPath = os.getenv("OUTPUT_BIN_PATH")
else:
    outputPath = projectSourcePath + '/../binaries/'

try:
	os.stat(outputPath)
except:
	os.mkdir(outputPath)

os.chdir(outputPath)
archivePath = 'archives'

try:
	os.stat(archivePath)
except:
	os.mkdir(archivePath)

for filename in os.listdir('.'):
	if os.path.isfile(filename):
		if re.match(projectOutputFilename, filename) is not None:
			print 'Archiving ' + filename
			os.rename(filename, archivePath + '/' + filename)

os.chdir(buildPath)

for filename in os.listdir('.'):
	if os.path.isfile(filename):
		if re.match(projectOutputFilename + projectOutputFilenameExtension, filename):
			print 'Copying ' + projectOutputFilename + versionTag + projectOutputFilenameExtension
			shutil.copyfile(filename, outputPath + '/' + projectOutputFilename + versionTag + projectOutputFilenameExtension)
