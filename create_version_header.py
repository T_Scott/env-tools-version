#!/usr/bin/python
 
# Python Version Header Generator
# Uses the git describe command to generate a C header containing version information
# Usage: `./create_version_header.py output.h`
 
import argparse
import fetch_version
import os
 
# Parse input argument
parser = argparse.ArgumentParser(description='Generate `git describe` based version header file')
parser.add_argument('output', type=str, help='output file name')
parser.add_argument('macroPrefix', type=str, help='prefix for version macros')
parser.add_argument('projectSourcePath', type=str, help='location of project base directory')
 
args = parser.parse_args()
output = args.output
macroPrefix = args.macroPrefix
projectSourcePath = args.projectSourcePath

# Extract version strings 
fullVersionString, versionString, majorVersionString, minorVersionString, patchVersionString, tweakVersionString, dirty = fetch_version.extract_git_version(projectSourcePath)

if dirty is True:
	dirtyString = 'true'
else:
	dirtyString = 'false'

# Generate new file string
headerString = ('#ifndef ' + macroPrefix + '_GIT_VERSION_H\r\n#define ' + macroPrefix + '_GIT_VERSION_H\r\n\r\n' + 
				'#define ' + macroPrefix + '_VERSION_STRING\t\"' 	+ fullVersionString + '\"\r\n' +
				'#define ' + macroPrefix + '_VERSION\t\t\t'			+ versionString + '\r\n' +
				'#define ' + macroPrefix + '_MAJOR_VERSION\t\t' 	+ majorVersionString + '\r\n' +
				'#define ' + macroPrefix + '_MINOR_VERSION\t\t' 	+ minorVersionString + '\r\n' +
				'#define ' + macroPrefix + '_PATCH_VERSION\t\t' 	+ patchVersionString + '\r\n' +
				'#define ' + macroPrefix + '_TWEAK_VERSION\t\t' 	+ tweakVersionString.zfill(1) + '\r\n' +
				'#define ' + macroPrefix + '_DIRTY_VERSION\t\t' 	+ dirtyString + '\r\n'
				'\r\n#endif\r\n'
)
 
# Load existing file string
try:
	f = open(output, 'r')
	f.seek(0)
	fileString = f.read();
	f.close();
except IOError, e:
	fileString = "";
 
# Check for differences and update if required
if fileString != headerString:
	f = open(output, 'w+')
	f.seek(0)
	f.write(headerString)
	f.truncate()
	f.close() 